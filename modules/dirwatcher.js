import fs from 'fs';
import eventsEmmiter from '../eventsEmmiter';

export default class DirWatcher {
  setLastTimeMs (time) {
    let prevTime = this.time || 0;
    this.checkIfChanged(prevTime, time);
    this.time = time || 0;
  }

  checkIfChanged (prevTime, nextTime) {
    if(prevTime === nextTime) {
      return;
    } else {
      if (prevTime !== 0) {
        eventsEmmiter.emit('dirwatcher:changed');
      }
    }
  }

  getStat (path) {
    if (process.argv.length <= 2) {
      process.exit(-1);
    }

    fs.stat(path, (err, stats) => {
      this.setLastTimeMs(stats.mtimeMs);
    });
  }

  watch(path, dely) {
    setInterval(()=>{
      this.getStat(path)
    }, dely);
  }
}