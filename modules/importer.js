import fs from 'fs';
import { promisify } from 'util';
import csv from 'csvtojson';

export default class Importer {
  import (path, filesLimit) {
    let files = [];

    return promisify(fs.readdir)(path).then((filenames) => {
       filenames.map((filename, index) => {
         if (filesLimit && index >= filesLimit ) return;

         const filepath = path + '/'+ filename;
         this.getCSV(filepath, files);
      })
      return files;
    })
  }

  importSync(path, filesLimit, cb) {
    const filenames = fs.readdirSync(path);
    let files = [];

    filenames.map((filename, index) => {
      const filepath = path + '/'+ filename;

      if (filesLimit && index >= filesLimit ) return;

      this.getCSV(filepath, files);
    })

    cb(files);
  }

  getCSV(filepath, files) {
     csv()
      .fromFile(filepath)
      .on('json',(jsonObj)=>{
        files.push(jsonObj)
      }).on('done', ()=> {
       return files;
     })
  }
}