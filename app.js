import DirWatcher from './modules/dirwatcher';
import eventsEmmiter from './eventsEmmiter';
import Importer from './modules/importer';

let dirWatcher = new DirWatcher();
dirWatcher.watch('./data', 3000);

eventsEmmiter.on('dirwatcher:changed', function () {
  const importer = new Importer();

  let data = importer.import('./data', 5); // 5 is an arbitrary number of CSV files

  data.then(data => {
    setTimeout(() => {
      console.log('Async data:', data);
    }, 100);
  })

  importer.importSync('./data', 5, (data) => {
    setTimeout(() => {
      console.log('Sync data:', data);
    }, 100);
  });
});

